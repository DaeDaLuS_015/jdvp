# Overview

This plugin for JIRA is a custom field that allows you to connect to an external database and fetch some values from there.
After adding this custom field to an issue edit screen, you can select one of those values so it will be associated with the current issue.

You can choose how the values are rendered for viewing, editing and searching independently. For the editing, you can use a combobox (single select or cascading select) or an AJAX-style input field.
Internally, the plugin will store only the primary key of the item you have selected, so you can safely edit things in your database, as long as you keep the primary key constant.

# Downloads

Binary releases of this JIRA plugin can be downloaded from the [Atlassian Marketplace](https://marketplace.atlassian.com/plugins/org.deblauwe.jira.plugin.database-values-plugin).

# Documentation

See [wiki](https://bitbucket.org/wimdeblauwe/jdvp/wiki/Home) for the full documentation on installation and configuration of the plugin.
